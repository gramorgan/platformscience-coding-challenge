# Platform Science Coding Challenge

This is my submission for the Platform Science coding challenge. It's a single-file Python script that reads two files 
specified as command-line arguments, one for street names and one for driver names. It then computes the 
"suitability-maximizing" matching between these as specified in the instructions and prints this matching along with
the maximum total suitability.

Also included are a suite of tests built using pytest.

It's built using [Poetry](https://python-poetry.org/) and can be run in Docker using the provided Dockerfile and `go.sh`
script.

## Build and Run

The only prerequisite for this project is [Docker](https://www.docker.com/) so make sure you have that installed before
starting. Once Docker is installed, simply run
```bash
./go.sh build
```
to build a docker image for the project with all
dependencies installed and configured.

Once the image is built, you can run the application with
```bash
./go.sh run [street-file] [driver-file]
```
This will automatically create a container, copy your input files into it and run the application.

To run the test suite, run
```bash
./go.sh test
```

## Manual Install

If you're interested in installing the project locally, you'll have to install Python 3.9. I recommend using
[pyenv](https://github.com/pyenv/pyenv) for this. Then you'll have to acquire Poetry, either through the install script
on their site or through `pip3 install poetry`. Once poetry is installed, just run
```bash
poetry install --no-root
```
to install the application and its dependencies. From there you can run the main script with
```bash
poetry run python main.py [street-file] [driver-file]
```
or run tests with
```bash
poetry run pytest -q test.py
```
