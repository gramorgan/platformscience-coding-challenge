import contextlib
import io
import random
import string

import pytest
import main


def test_gcd():
    assert main.gcd(16, 4) == 4
    assert main.gcd(6, 23) == 1
    assert main.gcd(12, 16) == 4
    assert main.gcd(105, 70) == 35


def test_count_vowels():
    assert main.count_num_vowels('') == 0
    assert main.count_num_vowels('aeiou') == 5
    assert main.count_num_vowels('AEIOU') == 5
    assert main.count_num_vowels('Platform Science') == 5
    assert main.count_num_vowels('Sp3c!al_ChaR@ct39z') == 2


def test_count_consonants():
    assert main.count_num_consonants('') == 0
    assert main.count_num_consonants('bcd') == 3
    assert main.count_num_consonants('BCD') == 3
    assert main.count_num_consonants('aeiou') == 0
    assert main.count_num_consonants('Platform Science') == 10
    assert main.count_num_consonants('Sp3c!al_ChaR@ct39z') == 10


def test_calc_suitability():
    # no vowels or consonants in driver name always means 0 suitability
    assert main.calc_suitability('', '') == 0
    assert main.calc_suitability('asdf', '+*4 72') == 0

    # even street name length, one vowel in driver name, lengths have a common factor > 1
    # 1 * 1.5 * 1.5 = 2.25
    assert main.calc_suitability('test', 'test') == 2.25
    # odd street name length, 5 consonants in driver name, no common factor
    # 5 * 1 = 5
    assert main.calc_suitability('123 ABC St.', 'Richard') == 5
    # even street name length, 4 vowels in driver name, lengths have a common factor > 1
    # 4 * 1.5 * 1.5 = 9
    assert main.calc_suitability('155 Durango Canyon', 'Charles M. Johnson') == 9


def test_compute_optimal_matching_bruteforce():
    """Run some simple tests against brute-force matching algorithm"""

    # differing-length inputs should raise ValueError
    with pytest.raises(ValueError):
        main.compute_optimal_matching_bruteforce(
            ['a', 'b'],
            ['c']
        )

    # only one possible matching
    total_suitability, matching = main.compute_optimal_matching_bruteforce(
        ['155 Durango Canyon'],
        ['Charles M. Johnson']
    )
    assert total_suitability == 9
    assert set(matching) == {
        ('155 Durango Canyon', 'Charles M. Johnson'),
    }

    # even-length street name should be matched with all-vowel driver
    # odd-length street name should be matched with all-consonant driver
    total_suitability, matching = main.compute_optimal_matching_bruteforce(
        ['123', '1234'],
        ['eeeee', 'ttttt']
    )
    assert total_suitability == 12.5
    assert set(matching) == {
        ('123', 'ttttt'),
        ('1234', 'eeeee')
    }

    total_suitability, matching = main.compute_optimal_matching_bruteforce(
        ['a', 'bb', 'c'],
        ['abC', 'ABa', 'A BC'],
    )
    assert total_suitability == 7
    assert set(matching) == {
        ('bb', 'ABa'),
        ('c', 'A BC'),
        ('a', 'abC'),
    }


def test_compute_optimal_matching_scipy():
    """Run some simple tests against scipy matching algorithm"""

    # differing-length inputs should raise ValueError
    with pytest.raises(ValueError):
        main.compute_optimal_matching_scipy(
            ['a', 'b'],
            ['c']
        )

    # only one possible matching
    total_suitability, matching = main.compute_optimal_matching_scipy(
        ['155 Durango Canyon'],
        ['Charles M. Johnson']
    )
    assert total_suitability == 9
    assert set(matching) == {
        ('155 Durango Canyon', 'Charles M. Johnson'),
    }

    # even-length street name should be matched with all-vowel driver
    # odd-length street name should be matched with all-consonant driver
    total_suitability, matching = main.compute_optimal_matching_scipy(
        ['123', '1234'],
        ['eeeee', 'ttttt']
    )
    assert total_suitability == 12.5
    assert set(matching) == {
        ('123', 'ttttt'),
        ('1234', 'eeeee')
    }

    total_suitability, matching = main.compute_optimal_matching_scipy(
        ['a', 'bb', 'c'],
        ['abC', 'ABa', 'A BC'],
    )
    assert total_suitability == 7
    assert set(matching) == {
        ('bb', 'ABa'),
        ('c', 'A BC'),
        ('a', 'abC'),
    }


def generate_random_string():
    """Generates a random string suitable for use as a test street or driver name

    Strings are of random length from 3-15 characters and consist of printable ascii characters"""

    output_len = random.randint(3, 15)
    return ''.join(random.choices(string.printable, k=output_len))


def test_validate_bruteforce_scipy_equivalent():
    """Compare results of brute-force and scipy implementations

    Generate random inputs and ensure that both algorithms compute the same total suitability for each input. We can't
    compare the actual matchings because different algorithms might give different results if there are multiple
    max-value matchings. We're OK with that as long as they give the same total suitability."""

    num_repetitions = 100
    for _ in range(num_repetitions):
        input_size = random.randint(2, 6)

        destination_names = [generate_random_string() for _ in range(input_size)]
        driver_names = [generate_random_string() for _ in range(input_size)]

        bruteforce_total_suitability, _ = main.compute_optimal_matching_bruteforce(destination_names, driver_names)
        scipy_total_suitability, _ = main.compute_optimal_matching_scipy(destination_names, driver_names)

        assert bruteforce_total_suitability == scipy_total_suitability


CLI_TEST_EXPECTED_RESULT = '''Total suitability: 12.5
"ttttt" -> "123"
"aaaaa" -> "1234"
'''


def test_cli():
    """Test that CLI interface properly reads files and formats output"""
    with contextlib.redirect_stdout(io.StringIO()) as f:
        main.main(['test_inputs/streetnames1.txt', 'test_inputs/drivernames1.txt'])

    assert f.getvalue() == CLI_TEST_EXPECTED_RESULT
