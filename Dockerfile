FROM python:3.9-buster

WORKDIR /app
COPY . .
RUN pip install poetry==1.4.0 && poetry install --no-root
