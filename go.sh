#!/bin/bash

set -e

IMAGE_NAME='mg-coding-challenge'

case $1 in
build)
  docker build -t $IMAGE_NAME .
  ;;
run)
  if [[ ! -f $2 ]] || [[ ! -f $3 ]]; then
    echo 'Must supply two files as input'
    exit 2
  fi

  CONTAINER=$(docker container create $IMAGE_NAME \
    poetry run python main.py '/tmp/streetnames.txt' '/tmp/drivernames.txt')

  docker cp "$2" "$CONTAINER:/tmp/streetnames.txt"
  docker cp "$3" "$CONTAINER:/tmp/drivernames.txt"

  docker start -a "$CONTAINER"
  docker rm "$CONTAINER"
  ;;
test)
  docker run --rm $IMAGE_NAME poetry run pytest -q test.py
  ;;
*)
  echo "Invalid option \"$1\""
  exit 1
  ;;
esac
