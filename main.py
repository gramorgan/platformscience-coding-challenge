import itertools
import sys
from pathlib import Path
from typing import List

import numpy as np
from scipy.optimize import linear_sum_assignment

# FIXME: is 'y' a vowel?
VOWELS = {'a', 'e', 'i', 'o', 'u'}


def gcd(a: int, b: int) -> int:
    """Calculate the greatest common divisor of `a` and `b`

    Uses euclidian algorithm"""
    while b != 0:
        t = b
        b = a % b
        a = t
    return a


def count_num_vowels(s: str) -> int:
    """Count the number of vowels in `s`"""
    return len([c for c in s.lower() if c in VOWELS])


def count_num_consonants(s: str) -> int:
    """Count the number of consonants in `s`"""
    return len([c for c in s.lower() if c.isalpha() and c not in VOWELS])


def calc_suitability(destination_street_name: str, driver_name: str) -> float:
    """Calculate suitability score for a single driver/destination pairing"""
    street_name_length = len(destination_street_name)
    driver_name_length = len(driver_name)

    if street_name_length % 2 == 0:
        base_suitability = count_num_vowels(driver_name) * 1.5
    else:
        base_suitability = count_num_consonants(driver_name)

    # gcd(a, b) > 1 means a and b share at least one common factor that isn't 1
    if gcd(street_name_length, driver_name_length) > 1:
        return base_suitability * 1.5

    return base_suitability


def compute_optimal_matching_bruteforce(destination_street_names: List[str], driver_names: List[str]):
    """Compute an optimal matching for a set of street and driver names using brute force algorithm

    This algorithm computes all possible permutations of the driver names list and matches them with
    destination names by index to enumerate every possible matching between destinations and drivers.
    This runs in n! time, so is only viable for relatively short lists. This is used to validate
    the faster scipy implementation."""
    if len(destination_street_names) != len(driver_names):
        raise ValueError('Expected equal number of street names and driver names')

    highest_total_suitability = float('-inf')
    highest_suitability_ordering = None

    for driver_names_ordering in itertools.permutations(driver_names):
        driver_names_ordering_list = list(driver_names_ordering)
        total_suitability = sum(
            calc_suitability(street_name, driver_name)
            for street_name, driver_name in zip(destination_street_names, driver_names_ordering_list)
        )
        if total_suitability > highest_total_suitability:
            highest_total_suitability = total_suitability
            highest_suitability_ordering = driver_names_ordering_list

    pairings = list(zip(destination_street_names, highest_suitability_ordering))
    return highest_total_suitability, pairings


def compute_optimal_matching_scipy(destination_street_names: List[str], driver_names: List[str]):
    """Compute an optimal matching for a set of street and driver names using scipy's linear_sum_assignment

    It uses the "Jonker-Volgenant" algorithm to solve the assignment problem in polynomial time. This is the
    matching routine that's invoked when this application is run from the command line."""
    if len(destination_street_names) != len(driver_names):
        raise ValueError('Expected equal number of street names and driver names')

    # calculate suitability for every combination of street name and driver name
    suitability_matrix = np.array([
        [
            calc_suitability(destination_street_name, driver_name)
            for driver_name in driver_names
        ]
        for destination_street_name in destination_street_names
    ])

    # use scipy's `linear_sum_assignment` to compute max-suitability pairings
    # for more info see https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linear_sum_assignment.html
    street_name_indices, driver_name_indices = linear_sum_assignment(suitability_matrix, maximize=True)

    total_suitability = suitability_matrix[street_name_indices, driver_name_indices].sum()
    pairings = [
        (destination_street_names[street_name_index], driver_names[driver_name_index])
        for street_name_index, driver_name_index
        in zip(street_name_indices, driver_name_indices)
    ]

    return total_suitability, pairings


def main(args):
    if len(args) != 2:
        print('Error: please supply exactly two input files')
        return 1

    street_names_filename, driver_names_filename = args

    street_names = Path(street_names_filename).read_text().strip().split('\n')
    driver_names = Path(driver_names_filename).read_text().strip().split('\n')

    total_suitability, pairings = compute_optimal_matching_scipy(street_names, driver_names)

    print('Total suitability:', total_suitability)
    for street_name, driver_name in pairings:
        print(f'"{driver_name}"', '->', f'"{street_name}"')


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
